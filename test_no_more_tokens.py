# Результат данного теста зависит от того, прошли ли тесты `auth` и `tokens`,
# поэтому вызывается он лишь в том случае, когда все предыдущие тесты для API
# пройдены.
import os
from jsonschema import validate

from api_tester import get_response, load_schema
from test_auth import AUTH_SCHEMAS_PATH
from test_auth import URI as AUTH_URI
from test_tokens import URI as TOKENS_URI

AUTH_403_SCHEMA = AUTH_SCHEMAS_PATH + '403.json'


def test_no_more_tokens():
    # получаем новый токен
    status_code, body_json = get_response('GET', AUTH_URI, auth_type='Basic')
    assert status_code == 200
    token = body_json['token']
    os.environ['ORIOKS_TOKEN'] = token
    tokens = [token]  # для аннулирования токенов
    # получаем список токенов
    status_code, body_json = get_response('GET', TOKENS_URI)
    assert status_code == 200
    n_tokens = len(body_json)
    # получаем токены до предела из 8-ми токенов
    for i in range(n_tokens, 8):
        status_code, body_json = get_response(
            'GET', AUTH_URI, auth_type='Basic')
        assert status_code == 200
        tokens.append(body_json['token'])
    status_code, body_json = get_response('GET', AUTH_URI, auth_type='Basic')
    assert status_code == 403
    assert validate(body_json, load_schema(AUTH_403_SCHEMA)) is None
    # отзываем полученные для данного теста токены
    for x in reversed(tokens):  # токен для аннулирования идёт последним
        status_code, _ = get_response('DELETE', '{}/{}'.format(TOKENS_URI, x))
