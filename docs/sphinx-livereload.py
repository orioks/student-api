#!/usr/bin/env python
from argparse import ArgumentParser

from formic import FileSet
from livereload import Server, shell


def setup_parser(parser):
    parser.add_argument(
        '--force-rebuild',
        action='store_true',
        help='Пересбока документации с нуля',
    )
    parser.add_argument(
        '--watch-all',
        action='store_true',
        help='Отлеживать изменения во всех файлах',
    )
    return parser


def main():
    parser = setup_parser(ArgumentParser())
    args = parser.parse_args()
    if args.watch_all:
        files = FileSet(
            include='**/*',
            exclude=[
                '_build/**',
                'Pipfile*',
                'README.md',
                'Makefile',
                'make.bat',
            ],
        )
    else:
        files = FileSet(include='**/*.rst')
    if args.force_rebuild:
        command = 'make clean && make html'
    else:
        command = 'make html'
    server = Server()
    for file_ in files:
        server.watch(file_, shell(command, shell=True))
    server.serve(root='_build/html')


if __name__ == '__main__':
    main()
