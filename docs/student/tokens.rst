Просмотр токенов
================


Запрос
------

.. code-block:: http

   GET /api/v1/student/tokens HTTP/1.1
   Accept: application/json
   Authorization: Bearer <token>
   User-Agent: <app>/<app-version> <os>[ <os-version>]

.. hint:: Информация о заполнении заголовков находится :ref:`здесь
   <filling_headers>`.


Ответ
-----

.. code-block:: http

   HTTP/1.1 200 OK

.. code-block:: json

   [
     {
       "last_used": "String",
       "token": "String",
       "user_agent": "String"
     }
   ]


:ref:`JSONSchema <tokens_GET_200>` для валидации ответа.

``last_used``
   Время последнего использования токена в формате ``YYYY-MM-DDTHH:mm``
   (`ISO 8601`_).

.. _ISO 8601: https://en.wikipedia.org/wiki/ISO_8601

``token`` и ``user_agent``
   Смотри :ref:`аутентификацию <filling_headers>`.

Пример
------

.. code-block:: http

   GET /api/v1/student/tokens HTTP/1.1
   Accept: application/json
   Authorization: Bearer T7d7IyzKKO4ehP5Imuz0Hg1EJfcsY8QX
   User-Agent: orioks_api_test/0.1 GNU/Linux 4.17.2-1-ARCH

.. code-block:: http

   HTTP/1.1 200 OK

.. code-block:: json

   [
     {
       "last_used": "2018-08-06T16:35",
       "token": "T7d7IyzKKO4ehP5Imuz0Hg1EJfcsY8QX",
       "user_agent": "orioks_api_test/0.1 GNU/Linux 4.17.2-1-ARCH"
     }
   ]

.. seealso:: В разделе :doc:`ЧаВо</faq>` указаны все коды ошибок, которые
   возвращает API.


.. _token_revocation:

Аннулирование токенов
=====================


Запрос
------

.. code-block:: http

   DELETE /api/v1/student/tokens/<token> HTTP/1.1
   Accept: application/json
   Authorization: Bearer <token>
   User-Agent: <app>/<app-version> <os>[ <os-version>]

``<token>``
   Токен :ref:`аутентификации <filling_headers>`.


Ответ
-----

.. code-block:: http

   HTTP/1.1 200 OK

.. code-block:: json

   {
     "info": "Токен успешно аннулирован"
   }

:ref:`JSONSchema <tokens_DELETE_200>` для валидации ответа.

``info``
   Описание успешно выполненной операции.


Пример
------

.. code-block:: http

   DELETE /api/v1/student/tokens/T7d7IyzKKO4ehP5Imuz0Hg1EJfcsY8QX HTTP/1.1
   Accept: application/json
   Authorization: Bearer T7d7IyzKKO4ehP5Imuz0Hg1EJfcsY8QX
   User-Agent: orioks_api_test/0.1 GNU/Linux 4.17.2-1-ARCH

.. code-block:: http

   HTTP/1.1 200 OK

.. code-block:: json

   {
     "info": "Токен успешно аннулирован"
   }

Возможные ошибки
----------------

.. code-block:: http

   HTTP/1.1 400 Bad Request

.. code-block:: json

   {
     "error": "Токен уже аннулирован или не существует"
   }

:ref:`JSONSchema <tokens_DELETE_400_wrong_token>` для валидации ответа.

.. seealso:: В разделе :doc:`ЧаВо</faq>` указаны все коды ошибок, которые
   возвращает API.
