Академические задолженности
===========================


Запрос
------

.. code-block:: http

   GET /api/v1/student/academic-debts HTTP/1.1
   Accept: application/json
   Authorization: Bearer <token>
   User-Agent: <app>/<app-version> <os>[ <os-version>]

.. hint:: Информация о заполнении заголовков находится :ref:`здесь
   <filling_headers>`.

.. _academic_debt_id:


Ответ
-----

.. code-block:: http

   HTTP/1.1 200 OK

.. code-block:: json

   [
     {
       "consultation_schedule": "String, Optional",
       "control_form": "String, Optional",
       "current_grade": "Float, Optional",
       "deadline": "String, Optional",
       "department": "String",
       "id": "Integer",
       "max_grade": "Float",
       "name": "String",
       "teachers": [ "String, Optional" ]
     }
   ]

.. note:: 
   Долги со всех семестров объединяются в один список. Если получен
   пустой список, значит у студента нет долгов.


``consultation_schedule``
   Расписание консультаций, см. пример ниже.

``control_form``
   Форма контроля. Если ключ отсутствует, следует выводить "Не назначено".

.. code-block:: json

   [
     "Зачёт", 
     "Дифференцированный зачёт", 
     "Экзамен", 
     "Курсовая работа", 
     "Курсовой проект", 
     "Защита ВКР"
   ]

``current_grade``
   Текущий балл по академической задолженности. Если ключ отсутствует, следует
   выводить ``-`` (дефис).

``deadline``
   Крайняя дата ликвидации в формате ``YYYY-MM-DD`` (`ISO 8601`_). Если ключ
   отсутствует, cледует выводить "Не назначена" или вовсе скрывать поле.

.. _ISO 8601: https://en.wikipedia.org/wiki/ISO_8601

``department``
   Кафедра, к которой относится академическая задолженность.

``id``
   Уникальный идентификатор академической задолженности.

``max_grade``
   Максимальный балл по академической задолженности.

``name``
   Название академической задолженности.

``teachers``
   ФИО преподавател(я/ей). Если ключ отсутствует, следует выводить "Не
   назначены" или вовсе скрывать поле.


Пример
------

.. code-block:: http

   GET /api/v1/student/academic-debts HTTP/1.1
   Accept: application/json
   Authorization: Bearer T7d7IyzKKO4ehP5Imuz0Hg1EJfcsY8QX
   User-Agent: orioks_api_test/0.1 GNU/Linux 4.17.2-1-ARCH

.. code-block:: http

   HTTP/1.1 200 OK

.. code-block:: json

   [
     {
       "consultation_schedule": "Каждую неделю, Понедельник, 9:00",
       "control_form": "Зачёт",
       "current_grade": 0.0,
       "deadline": "2019-01-31",
       "department": "КИТиС",
       "id": 132,
       "max_grade": 100.0,
       "name": "Компьютерная Практика",
       "teachers": [ "Соколова Натэлла Юрьевна" ]
     }
   ]

.. seealso:: В разделе :doc:`ЧаВо</faq>` указаны все коды ошибок, которые
   возвращает API.
