Информация о студенте
=====================

.. для того, чтобы в боковой панели документ отображался как вложенный

.. toctree::
   :hidden:

   academic_debts
   resits
   disciplines
   events
   pushes
   tokens


Запрос
------

.. code-block:: http

   GET /api/v1/student HTTP/1.1
   Accept: application/json
   Authorization: Bearer <token>
   User-Agent: <app>/<app-version> <os>[ <os-version>]

.. hint:: Информация о заполнении заголовков находится :ref:`здесь
   <filling_headers>`.


Ответ
-----

.. code-block:: http

   HTTP/1.1 200 OK

.. code-block:: json

   {
     "course": "Integer",
     "department": "String",
     "full_name": "String",
     "group": "String",
     "record_book_id": "Integer",
     "semester": "Integer",
     "study_direction": "String",
     "study_profile": "String",
     "year": "String"
   }

:ref:`JSONSchema <student_GET_200>` для валидации ответа.

``course``
   Курс, на котором сейчас находится студент.

``department``
   Кафедра студента.

``full_name``
   ФИО студента.

``group``
   Учебная группа.

``record_book_id``
   Номер зачётной книжки студента.

``semester``
   Текущий семестр в этом учебном году (первый или второй).

``study_direction``
   Направление подготовки.

``study_profile``
   Профиль обучения.

``year``
   Текущий учебный год.

Пример
------

.. code-block:: http

   GET /api/v1/student HTTP/1.1
   Accept: application/json
   Authorization: Bearer T7d7IyzKKO4ehP5Imuz0Hg1EJfcsY8QX
   User-Agent: orioks_api_test/0.1 GNU/Linux 4.17.2-1-ARCH

.. code-block:: http

   HTTP/1.1 200 OK

.. code-block:: json

   {
     "course": 1,
     "department": "ИПОВС",
     "full_name": "Иванов Иван Иванович",
     "group": "МП-15",
     "record_book_id": 8120843,
     "semester": 2,
     "study_direction": "Программная инженерия",
     "study_profile": "Программные технологии распределенной обработки информации",
     "year": "2017-2018"
   }

.. seealso:: В разделе :doc:`ЧаВо</faq>` указаны все коды ошибок, которые
   возвращает API.
