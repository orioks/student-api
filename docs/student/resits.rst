Пересдачи академической задолженности
=====================================


Запрос
------

.. code-block:: http

   GET /api/v1/student/academic-debts/<academic-debt-id>/resits HTTP/1.1
   Accept: application/json
   Authorization: Bearer <token>
   User-Agent: <app>/<app-version> <os>[ <os-version>]

``<academic-debt-id>``
   :ref:`Идентификатор <academic_debt_id>` академической задолженности.


.. hint:: Информация о заполнении заголовков находится :ref:`здесь
   <filling_headers>`.


Ответ
-----

.. code-block:: http

   HTTP/1.1 200 OK

.. code-block:: json

   [
     {
       "classroom": "String",
       "datetime": "String",
       "resit_number": "Integer"
     }
   ]

.. note:: Если получен пустой список, значит пересдачи не назначены.

``classroom``
   Аудитория, в которой будет проходить пересдача.

``datetime``
   Дата и время пересдачи в формате ``YYYY-MM-DDTHH:mm`` (`ISO 8601`_).

.. _ISO 8601: https://en.wikipedia.org/wiki/ISO_8601

``resit_number``
   Номер пересдачи.


Пример
------

.. code-block:: http

   GET /api/v1/student/academic-debts/132/resits HTTP/1.1
   Accept: application/json
   Authorization: Bearer T7d7IyzKKO4ehP5Imuz0Hg1EJfcsY8QX
   User-Agent: orioks_api_test/0.1 GNU/Linux 4.17.2-1-ARCH

.. code-block:: http

   HTTP/1.1 200 OK

.. code-block:: json

   [
     {
       "classroom": "4232",
       "datetime": "2018-03-14T16:00",
       "resit_number": 1
     }
   ]


Возможные ошибки
----------------

.. code-block:: http

   HTTP/1.1 400 Bad Request

.. code-block:: json

   {
     "error": "Академической задолженности с данным идентификатором не существует"
   }

.. seealso:: В разделе :doc:`ЧаВо</faq>` указаны все коды ошибок, которые
   возвращает API.
