
Подписка на уведомления
===================================


Запрос
------

.. code-block:: http

   POST /api/v1/student/pushes/register HTTP/1.1
   Accept: application/json
   Authorization: Bearer <token>
   User-Agent: <app>/<app-version> <os>[ <os-version>]
  
.. code-block:: json

   {
	    "deviceUuid": String,
    	"pushKey": String,
	    "deviceType": Int
   }

``<deviceType>``
   :ref:`Идентификатор <deviceType>` определяет тип устройства. 0 - Android, 1 - iOS.

.. hint:: Информация о заполнении заголовков находится :ref:`здесь
   <filling_headers>`.

Ответ
-----

.. code-block:: http

   HTTP/1.1 200 OK


Отписка от уведомлений
===================================


Запрос
------

.. code-block:: http

   POST /api/v1/student/pushes/unregister HTTP/1.1
   Accept: application/json
   Authorization: Bearer <token>
   User-Agent: <app>/<app-version> <os>[ <os-version>]
  
.. code-block:: json

   {
	    "deviceUuid": String
   }

.. hint:: Информация о заполнении заголовков находится :ref:`здесь
   <filling_headers>`.

Ответ
-----

.. code-block:: http

   HTTP/1.1 200 OK



TODO: JSONSchema

.. seealso:: В разделе :doc:`ЧаВо</faq>` указаны все коды ошибок, которые
   возвращает API.
