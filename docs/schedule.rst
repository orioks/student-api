Расписание (в разработке)
=========================


Получение текущего типа недели
------------------------------

Запрос
^^^^^^

.. code-block:: http

   GET /api/v1/schedule HTTP/1.1
   Accept: application/json
   Authorization: Bearer <token>
   User-Agent: <app>/<app-version> <os>[ <os-version>]

.. hint:: Информация о заполнении заголовков находится :ref:`здесь
   <filling_headers>`.


Ответ
^^^^^

.. code-block:: http

   HTTP/1.1 200 OK

.. code-block:: json

   {
     "semester_start": "String",
     "session_start": "String, Optional",
     "session_end": "String, Optional",
     "next_semester_start": "String, Optional"
   }

``semester_start``
  Дата начала семестра. Используется для определения номера текущей недели и
  типа недели.

``session_start``
  Дата начала сессии (известна к середине семестра).

``session_end``
  Дата конца сессии и начала каникул.

``next_semester_start``
  Дата начала следующего семестра (появляется к концу семестра).

Все даты передаются в формате ``YYYY-MM-DD`` (`ISO 8601`_). Во время сессии
и каникул вместо номера недели и её типа нужно отображать слово ``Сессия`` и
``Каникулы`` соотвественно.

.. _ISO 8601: https://en.wikipedia.org/wiki/ISO_8601

Определение номера и типа текущей недели:

.. code-block:: python

   import datetime as dt

   semester_start = dt.datetime.fromisoformat('2019-02-11')
   timedelta = dt.datetime.now() - semester_start
   current_week = (timedelta.days // 7) + 1
   n = (current_week - 1) % 4

+---+--------------------+
| n | Тип недели         |
+===+====================+
| 0 | Первый числитель   |
+---+--------------------+
| 1 | Первый знаменатель |
+---+--------------------+
| 2 | Второй числитель   |
+---+--------------------+
| 3 | Второй знаменатель |
+---+--------------------+


.. _groups:

Получение списка групп
----------------------

Запрос
^^^^^^

.. code-block:: http

   GET /api/v1/schedule/groups HTTP/1.1
   Accept: application/json
   Authorization: Bearer <token>
   User-Agent: <app>/<app-version> <os>[ <os-version>]

.. hint:: Информация о заполнении заголовков находится :ref:`здесь
   <filling_headers>`.


Ответ
^^^^^

.. code-block:: http

   HTTP/1.1 200 OK

.. code-block:: json

   [
     {
       "id": "Int",
       "name": "String"
     }
   ]

Например:

.. code-block:: json

   [
     {
       "id": "1",
       "name": "ПИН-11М (ИПОВС-11)"
     }
   ]

``id``
  Идентификатор группы.

``name``
  Имя группы.


Получение времени начала и конца всех пар
-----------------------------------------


.. _timetable:

Запрос
^^^^^^

.. code-block:: http

   GET /api/v1/schedule/timetable HTTP/1.1
   Accept: application/json
   Authorization: Bearer <token>
   User-Agent: <app>/<app-version> <os>[ <os-version>]

.. hint:: Информация о заполнении заголовков находится :ref:`здесь
   <filling_headers>`.


Ответ
^^^^^

.. code-block:: http

   HTTP/1.1 200 OK

.. code-block:: json

   {
     "String": ["String, String"]
   }

В массиве находится время начала и конца конкретной пары. Время указано в
формате ``HH:mm`` (`ISO 8601`_).

.. _ISO 8601: https://en.wikipedia.org/wiki/ISO_8601

Например:

.. code-block:: json

   {
     "1": ["09:00", "10:30"],
     "2": ["10:40", "12:10"],
     "3": ["12:20", "13:50"],
     "4": ["14:30", "16:00"],
     "5": ["16:10", "17:40"],
     "6": ["18:20", "19:50"],
     "7": ["20:00", "21:30"]
   }


Расписание конкретной группы
----------------------------


Запрос
^^^^^^

.. code-block:: http

   GET /api/v1/schedule/groups/<group-id> HTTP/1.1
   Accept: application/json
   Authorization: Bearer <token>
   User-Agent: <app>/<app-version> <os>[ <os-version>]

``<group-id>``
  Идентификатор текущей группы студента можно получить :ref:`здесь <groups>`.

.. hint:: Информация о заполнении заголовков находится :ref:`здесь
   <filling_headers>`.


Ответ
^^^^^

.. code-block:: http

   HTTP/1.1 200 OK

.. code-block:: json

   {
     "last_updated": "String",
     "semester": "String",
     "String": {
       "monday": {
         "String": {
           "classroom": "String",
           "name": "String",
           "teacher": "String",
           "teacher_initials": "String",
           "type": "String"
       }
     }
   }

Первый уровень цифр начинается с нуля и отвечает за тип недели. Далее идут дни
недели на английском языке со строчной буквы. Второй уровень цифр -- это номера
пар, которые нужно соотнести с их временем из :ref:`timetable <timetable>`.

``last_updated``
  Дата и время последнего обновления расписания для текущей группы в формате
  ``YYYY:MM:DDTHH:mm`` (`ISO 8601`_).

.. _ISO 8601: https://en.wikipedia.org/wiki/ISO_8601

``semester``
  Информация о текущем семестре.

``classroom``
  Номер аудитории.

``name``
  Название дисциплины.

``teacher``
  ФИО преподавателя.

``teacher_initials``
  Фамилия и инициалы преподавателя.

``type``
  Тип пары:

.. code-block:: json

   [
     "Лекция",
     "Семинар",
     "Лабораторная работа"
   ]

Пример
^^^^^^

.. code-block:: http

   GET /api/v1/schedule/groups/1 HTTP/1.1
   Accept: application/json
   Authorization: Bearer T7d7IyzKKO4ehP5Imuz0Hg1EJfcsY8QX
   User-Agent: orioks_api_test/0.1 GNU/Linux 4.17.2-1-ARCH

.. code-block:: http

   HTTP/1.1 200 OK

.. code-block:: json

   {
     "last_updated": "2019-02-24T14:00",
     "semester": "Весенний семестр 2018-2019 г.",
     "0": {
       "tuesday": {
         "4": {
           "classroom": "4338 y",
           "name": "Архитектура высокопроизводительных вычислительных систем",
           "teacher": "Дикарев Николай Иванович",
           "teacher_initials": "Дикарев Н.И.",
           "type": "Лекция"
         },
         "6": {
           "classroom": "1204 (м)",
           "name": "Компьютерные технологии в науке и образовании",
           "teacher": "Портнов Евгений Михайлович",
           "teacher_initials": "Портнов Е.М.",
           "type": "Лекция"
         }
       }
     }
   }


Проверка необходимости запроса расписания
-----------------------------------------

Запрос
^^^^^^

.. code-block:: http

   GET /api/v1/schedule/groups/<group-id>/last_updated HTTP/1.1
   Accept: application/json
   Authorization: Bearer <token>
   User-Agent: <app>/<app-version> <os>[ <os-version>]

``<group-id>``
  Идентификатор текущей группы студента можно получить :ref:`здесь <groups>`.

.. hint:: Информация о заполнении заголовков находится :ref:`здесь
   <filling_headers>`.


Ответ
^^^^^

.. code-block:: http

   HTTP/1.1 200 OK

.. code-block:: json

   {
     "last_updated": "String"
   }

``last_updated``
  Дата и время последнего обновления расписания для текущей группы в формате
  ``YYYY:MM:DDTHH:mm`` (`ISO 8601`_).

.. _ISO 8601: https://en.wikipedia.org/wiki/ISO_8601


Возможные ошибки
----------------

.. code-block:: http

   HTTP/1.1 400 Bad Request

.. code-block:: json

   {
     "error": "Группы с данным идентификатором не сущестует"
   }

TODO: JSONSchema


.. seealso:: В разделе :doc:`ЧаВо</faq>` указаны все коды ошибок, которые
   возвращает API.
