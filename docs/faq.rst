Часто Задаваемые Вопросы (ЧаВо)
===============================


Внесение несовместимых изменений в API
--------------------------------------

Если в API вносятся изменения, несовместимые с предыдущей версией, то создаётся
новый URI, например ``v2``. Старый URI продолжает функционировать в течение
нескольких месяцев, чтобы дать разработчикам время на обновление своих
приложений. Затем старая версия API отключается. При попытке обращения к любому
URI старой версии API отвечает кодом ``410 Gone``. Это сделано для того, чтобы
не обновлённые приложения выдавали ошибку *"приложение устарело"*, вместо
*"ОРИОКС временно не доступен"*.


Как мне следить за изменениями в документации?
----------------------------------------------

Нажми на ⭐ на главной странице `репозитория`_.

.. _репозитория: https://gitlab.com/orioks/student-api


Ошибки 4xx
----------

Все ошибки, отправляемые клиенту, имеют общий формат:

.. code-block:: json

   {
     "error": "description"
   }


400
^^^

Ошибка отправляется клиенту в случае отсутствия или неверного заполнения
заголовков ``Accept`` и/или ``User-Agent``. Варианты ``description``:

.. code-block:: json

   [
     "Отсутствует обязательный заголовок {}",
     "Недопустимый формат заголовка {}",
     "Отсутствует обязательный ключ {}",
     "Недопустимый формат ключа {}"
   ]

Где ``{}`` - заголовок или ключ, вызвавший ошибку. Например ``"Отсутствует
обязательный заголовок User-Agent"``.

Полный список JSONSchema для неправильных заголовков:

* Отсутствует обязательный заголовок Accept
  (:ref:`JSONSchema <common_ALL_400_no_accept_header>`).
* Недопустимый формат заголовка Accept
  (:ref:`JSONSchema <common_ALL_400_wrong_accept_header>`).
* Отсутствует обязательный заголовок User-Agent
  (:ref:`JSONSchema <common_ALL_400_no_useragent_header>`).
* Недопустимый формат заголовка User-Agent
  (:ref:`JSONSchema <common_ALL_400_wrong_useragent_header>`).


401
^^^

Ошибка отправляется в случае проблем с заголовком ``Authorization`` вне
зависимости от характера проблемы.

.. code-block:: http

   HTTP/1.1 401 Unauthorized

.. code-block:: json

   {
     "error": "Несуществующий или аннулированный токен"
   }

:ref:`JSONSchema <common_ALL_401_wrong_token>` для валидации ответа.


404
^^^

.. code-block:: http

   HTTP/1.1 404 Not Found

.. code-block:: json

   {
     "error": "Отсутствует ресурс по данному URI"
   }

:ref:`JSONSchema <common_ALL_404_resource_not_found>` для валидации ответа.


405
^^^

.. code-block:: http

   HTTP/1.1 405 Method Not Allowed

.. code-block:: json

   {
     "error": "Метод {} запрещён для данного ресурса"
   }

Где ``{}`` - один из `http-методов`_, например ``POST``.

.. _http-методов: https://www.w3.org/Protocols/rfc2616/rfc2616-sec9.html

Пример :ref:`JSONSchema <common_POST_405_post_not_allowed>`.


410
^^^

.. code-block:: http

   HTTP/1.1 410 Gone

.. code-block:: json

   {
     "error": "Данная версия API устарела"
   }

.. TODO JSONSchema


Все JSONSchema для валидации ответов API
========================================


Общие ошибки для невалидных заголовков
--------------------------------------


.. _common_ALL_400_no_accept_header:

Отсутствует обязательный заголовок Accept
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

.. include:: ../schemas/common_ALL_400_no_accept_header.json
   :code: json


.. _common_ALL_400_wrong_accept_header:

Недопустимый формат заголовка Accept
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

.. include:: ../schemas/common_ALL_400_wrong_accept_header.json
   :code: json


.. _common_ALL_400_no_useragent_header:

Отсутствует обязательный заголовок User-Agent
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

.. include:: ../schemas/common_ALL_400_no_useragent_header.json
   :code: json


.. _common_ALL_400_wrong_useragent_header:

Недопустимый формат заголовка User-Agent
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

.. include:: ../schemas/common_ALL_400_wrong_useragent_header.json
   :code: json


.. _common_ALL_401_wrong_token:

Несуществующий или аннулированный токен
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

.. include:: ../schemas/common_ALL_401_wrong_token.json
   :code: json


.. _common_ALL_404_resource_not_found:

Отсутствует ресурс по данному URI
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

.. include:: ../schemas/common_ALL_404_resource_not_found.json
   :code: json


.. _common_POST_405_post_not_allowed:

Метод запрещён для данного URI
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

.. include:: ../schemas/common_DELETE_405_delete_not_allowed.json
   :code: json

.. include:: ../schemas/common_PATCH_405_patch_not_allowed.json
   :code: json

.. include:: ../schemas/common_POST_405_post_not_allowed.json
   :code: json

.. include:: ../schemas/common_PUT_405_put_not_allowed.json
   :code: json


Получение токена
----------------


.. _auth_GET_200:

Успех
^^^^^

.. include:: ../schemas/auth_GET_200.json
   :code: json


.. _auth_GET_401_wrong_credentials:

Неверный логин или пароль
^^^^^^^^^^^^^^^^^^^^^^^^^

.. include:: ../schemas/auth_GET_401_wrong_credentials.json
   :code: json


.. _auth_GET_403_too_many_tokens:

Превышен лимит токенов
^^^^^^^^^^^^^^^^^^^^^^

.. include:: ../schemas/auth_GET_403_too_many_tokens.json
   :code: json


Получение данных студента
-------------------------


.. _student_GET_200:

Успех
^^^^^

.. include:: ../schemas/student_GET_200.json
   :code: json


Получение дисциплин студента
----------------------------


.. _disciplines_GET_200:

Успех
^^^^^

.. include:: ../schemas/disciplines_GET_200.json
   :code: json


Просмотр и удаление токенов
---------------------------


.. _tokens_GET_200:

Успешный просмотр
^^^^^^^^^^^^^^^^^

.. include:: ../schemas/tokens_GET_200.json
   :code: json


.. _tokens_DELETE_200:

Успешное удаление токена
^^^^^^^^^^^^^^^^^^^^^^^^

.. include:: ../schemas/tokens_DELETE_200.json
   :code: json


.. _tokens_DELETE_400_wrong_token:

Удаление несуществующего токена
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

.. include:: ../schemas/tokens_DELETE_400_wrong_token.json
   :code: json


Получение контрольных мероприятий дисциплины
--------------------------------------------


.. _events_GET_200:

Успех
^^^^^

.. include:: ../schemas/events_GET_200.json
   :code: json


.. _events_GET_400_wrong_discipline_id:

Неверный идентификатор дисциплины
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

.. include:: ../schemas/events_GET_400_wrong_discipline_id.json
   :code: json
