from jsonschema import validate

from api_tester import (get_response, load_schema,
                        NO_ACCEPT_SCHEMA, NO_USERAGENT_SCHEMA,
                        WRONG_ACCEPT_SCHEMA, WRONG_USERAGENT_SCHEMA,
                        WRONG_TOKEN_SCHEMA, PATCH_NOT_ALLOWED,
                        POST_NOT_ALLOWED, PUT_NOT_ALLOWED)
from test_student import STUDENT_SCHEMA_PATH

URI = 'https://orioks.miet.ru/api/v1/student/tokens'
RIGHT_DELETE_URI = '{}/'.format(URI)  # токен добавляется при выполнении тестов
WRONG_DELETE_URI = '{}/{}'.format(URI, '0' * 32)

TOKENS_SCHEMA_PATH = STUDENT_SCHEMA_PATH + 'tokens/'
TOKENS_200_SCHEMA = TOKENS_SCHEMA_PATH + '200.json'
DELETE_TOKENS_200_SCHEMA = TOKENS_SCHEMA_PATH + 'delete_200.json'
DELETE_TOKENS_400_SCHEMA = TOKENS_SCHEMA_PATH + 'delete_400.json'


def test_get_tokens_without_accept_header():
    status_code, body_json = get_response(
        'GET', URI, override_headers={'Accept': None})
    assert status_code == 400
    assert validate(body_json, load_schema(NO_ACCEPT_SCHEMA)) is None


def test_get_tokens_with_wrong_accept_header():
    status_code, body_json = get_response(
        'GET', URI, override_headers={'Accept': 'wrong'})
    assert status_code == 400
    assert validate(body_json, load_schema(WRONG_ACCEPT_SCHEMA)) is None


def test_get_tokens_without_useragent_header():
    status_code, body_json = get_response(
        'GET', URI, override_headers={'User-Agent': None})
    assert status_code == 400
    assert validate(body_json, load_schema(NO_USERAGENT_SCHEMA)) is None


def test_get_tokens_with_wrong_useragent_header():
    status_code, body_json = get_response(
        'GET', URI, override_headers={'User-Agent': 'wrong'})
    assert status_code == 400
    assert validate(body_json, load_schema(WRONG_USERAGENT_SCHEMA)) is None


def test_get_tokens_without_authorization_header():
    status_code, body_json = get_response(
        'GET', URI, override_headers={'Authorization': None})
    assert status_code == 401
    assert validate(body_json, load_schema(WRONG_TOKEN_SCHEMA)) is None


def test_get_tokens_with_wrong_credentials():
    status_code, body_json = get_response(
        'GET', URI, send_wrong_credentials=True)
    assert status_code == 401
    assert validate(body_json, load_schema(WRONG_TOKEN_SCHEMA)) is None


def test_tokens_with_wrong_post_method():
    status_code, body_json = get_response('POST', URI)
    assert status_code == 405
    assert validate(body_json, load_schema(POST_NOT_ALLOWED)) is None


def test_tokens_with_wrong_put_method():
    status_code, body_json = get_response('PUT', URI)
    assert status_code == 405
    assert validate(body_json, load_schema(PUT_NOT_ALLOWED)) is None


def test_tokens_with_wrong_patch_method():
    status_code, body_json = get_response('PATCH', URI)
    assert status_code == 405
    assert validate(body_json, load_schema(PATCH_NOT_ALLOWED)) is None


def test_get_tokens_with_right_credentials():
    status_code, body_json = get_response('GET', URI)
    assert status_code == 200
    assert validate(body_json, load_schema(TOKENS_200_SCHEMA)) is None
    global RIGHT_DELETE_URI
    # удаляем токен, полученный при тесте авторизации (т.е. последний)
    RIGHT_DELETE_URI += body_json[-1]['token']


def test_delete_tokens_without_accept_header_and_wrong_token():
    status_code, body_json = get_response(
        'DELETE', WRONG_DELETE_URI, override_headers={'Accept': None})
    assert status_code == 400
    assert validate(body_json, load_schema(NO_ACCEPT_SCHEMA)) is None


def test_delete_tokens_with_wrong_accept_header_and_wrong_token():
    status_code, body_json = get_response(
        'DELETE', WRONG_DELETE_URI, override_headers={'Accept': 'wrong'})
    assert status_code == 400
    assert validate(body_json, load_schema(WRONG_ACCEPT_SCHEMA)) is None


def test_delete_tokens_without_useragent_header_and_wrong_token():
    status_code, body_json = get_response(
        'DELETE', WRONG_DELETE_URI, override_headers={'User-Agent': None})
    assert status_code == 400
    assert validate(body_json, load_schema(NO_USERAGENT_SCHEMA)) is None


def test_delete_tokens_with_wrong_useragent_header_and_wrong_token():
    status_code, body_json = get_response(
        'DELETE', WRONG_DELETE_URI, override_headers={'User-Agent': 'wrong'})
    assert status_code == 400
    assert validate(body_json, load_schema(WRONG_USERAGENT_SCHEMA)) is None


def test_delete_tokens_without_authorization_header_and_wrong_token():
    status_code, body_json = get_response(
        'DELETE', WRONG_DELETE_URI, override_headers={'Authorization': None})
    assert status_code == 401
    assert validate(body_json, load_schema(WRONG_TOKEN_SCHEMA)) is None


def test_delete_tokens_with_wrong_credentials_and_wrong_token():
    status_code, body_json = get_response(
        'DELETE', WRONG_DELETE_URI, send_wrong_credentials=True)
    assert status_code == 401
    assert validate(body_json, load_schema(WRONG_TOKEN_SCHEMA)) is None


def test_delete_tokens_with_right_credentials_and_wrong_token():
    status_code, body_json = get_response('DELETE', WRONG_DELETE_URI)
    assert status_code == 400
    assert validate(body_json, load_schema(DELETE_TOKENS_400_SCHEMA)) is None


def test_delete_tokens_without_accept_header_and_right_token():
    status_code, body_json = get_response(
        'DELETE', RIGHT_DELETE_URI, override_headers={'Accept': None})
    assert status_code == 400
    assert validate(body_json, load_schema(NO_ACCEPT_SCHEMA)) is None


def test_delete_tokens_with_wrong_accept_header_and_right_token():
    status_code, body_json = get_response(
        'DELETE', RIGHT_DELETE_URI, override_headers={'Accept': 'wrong'})
    assert status_code == 400
    assert validate(body_json, load_schema(WRONG_ACCEPT_SCHEMA)) is None


def test_delete_tokens_without_useragent_header_and_right_token():
    status_code, body_json = get_response(
        'DELETE', RIGHT_DELETE_URI, override_headers={'User-Agent': None})
    assert status_code == 400
    assert validate(body_json, load_schema(NO_USERAGENT_SCHEMA)) is None


def test_delete_tokens_with_wrong_useragent_header_and_right_token():
    status_code, body_json = get_response(
        'DELETE', RIGHT_DELETE_URI, override_headers={'User-Agent': 'wrong'})
    assert status_code == 400
    assert validate(body_json, load_schema(WRONG_USERAGENT_SCHEMA)) is None


def test_delete_tokens_without_authorization_header_and_right_token():
    status_code, body_json = get_response(
        'DELETE', RIGHT_DELETE_URI, override_headers={'Authorization': None})
    assert status_code == 401
    assert validate(body_json, load_schema(WRONG_TOKEN_SCHEMA)) is None


def test_delete_tokens_with_wrong_credentials_and_right_token():
    status_code, body_json = get_response(
        'DELETE', RIGHT_DELETE_URI, send_wrong_credentials=True)
    assert status_code == 401
    assert validate(body_json, load_schema(WRONG_TOKEN_SCHEMA)) is None


def test_delete_tokens_with_right_credentials_and_right_token():
    status_code, body_json = get_response('DELETE', RIGHT_DELETE_URI)
    assert status_code == 200
    assert validate(body_json, load_schema(DELETE_TOKENS_200_SCHEMA)) is None
