import json
import os
from base64 import b64encode

import requests

SCHEMAS_PATH = 'schemas/'
NO_ACCEPT_SCHEMA = SCHEMAS_PATH + '400_no_accept_header.json'
NO_USERAGENT_SCHEMA = SCHEMAS_PATH + '400_no_useragent_header.json'
WRONG_ACCEPT_SCHEMA = SCHEMAS_PATH + '400_wrong_accept_header.json'
WRONG_USERAGENT_SCHEMA = SCHEMAS_PATH + '400_wrong_useragent_header.json'
WRONG_TOKEN_SCHEMA = SCHEMAS_PATH + '401_wrong_token.json'
DELETE_NOT_ALLOWED = SCHEMAS_PATH + '405_delete_not_allowed.json'
PATCH_NOT_ALLOWED = SCHEMAS_PATH + '405_patch_not_allowed.json'
POST_NOT_ALLOWED = SCHEMAS_PATH + '405_post_not_allowed.json'
PUT_NOT_ALLOWED = SCHEMAS_PATH + '405_put_not_allowed.json'


def get_response(http_method, uri, auth_type='Bearer',
                 override_headers={}, data={}, send_wrong_credentials=False):
    """Функция-комбаин для использования в тестах

    Для того, чтобы исключить заголовкок, нужно приравнять его к None.
    >>> get_response(http_method, ..., override_headers={'User-Agent': None})

    Словарь data используется для передачи QueryString, т.е.
    https://some.uri/get?key1=value1&key2=value2

    send_wrong_credentials вводит пароль (находится в переменной
    окружения ORIOKS_PASSWD) в обратном порядке для вызова ошибки
    авторизации.
    """
    methods = {'DELETE', 'GET', 'PATCH', 'POST', 'PUT'}
    assert http_method in methods
    headers = fill_headers(auth_type, send_wrong_credentials)
    headers.update(override_headers)
    r = requests.request(http_method, uri, headers=headers, data=data)
    return r.status_code, r.json()


def fill_headers(auth_type, send_wrong_credentials):
    assert auth_type in ('Basic', 'Bearer')
    if auth_type == 'Basic':
        login = os.environ.get('ORIOKS_LOGIN')
        assert login is not None
        passwd = os.environ.get('ORIOKS_PASSWD')
        assert passwd is not None
        if send_wrong_credentials:
            passwd = reversed(passwd)
        auth = b64encode(f'{login}:{passwd}'.encode()).decode()
        headers = {'Authorization': 'Basic ' + auth}
    else:
        token = os.environ.get('ORIOKS_TOKEN')
        if send_wrong_credentials:
            token = '0' * 32
        assert token is not None
        headers = {'Authorization': 'Bearer ' + token}
    accept = os.environ.get('ACCEPT_HEADER')
    assert accept is not None
    headers['Accept'] = accept
    user_agent = os.environ.get('USERAGENT_HEADER')
    assert user_agent is not None
    headers['User-Agent'] = user_agent
    return headers


def load_schema(json_schema_filename):
    assert os.path.exists(json_schema_filename)
    with open(json_schema_filename, 'r') as f:
        json_schema = json.load(f)
    return json_schema
