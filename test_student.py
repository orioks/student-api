from jsonschema import validate

from api_tester import (get_response, load_schema, SCHEMAS_PATH,
                        NO_ACCEPT_SCHEMA, NO_USERAGENT_SCHEMA,
                        WRONG_ACCEPT_SCHEMA, WRONG_USERAGENT_SCHEMA,
                        WRONG_TOKEN_SCHEMA, DELETE_NOT_ALLOWED,
                        PATCH_NOT_ALLOWED, POST_NOT_ALLOWED,
                        PUT_NOT_ALLOWED)

URI = 'https://orioks.miet.ru/api/v1/student'

STUDENT_SCHEMA_PATH = SCHEMAS_PATH + 'student/'
STUDENT_200_SCHEMA = STUDENT_SCHEMA_PATH + '200.json'


def test_student_without_accept_header():
    status_code, body_json = get_response(
        'GET', URI, override_headers={'Accept': None})
    assert status_code == 400
    assert validate(body_json, load_schema(NO_ACCEPT_SCHEMA)) is None


def test_student_with_wrong_accept_header():
    status_code, body_json = get_response(
        'GET', URI, override_headers={'Accept': 'wrong'})
    assert status_code == 400
    assert validate(body_json, load_schema(WRONG_ACCEPT_SCHEMA)) is None


def test_student_without_useragent_header():
    status_code, body_json = get_response(
        'GET', URI, override_headers={'User-Agent': None})
    assert status_code == 400
    assert validate(body_json, load_schema(NO_USERAGENT_SCHEMA)) is None


def test_student_with_wrong_useragent_header():
    status_code, body_json = get_response(
        'GET', URI, override_headers={'User-Agent': 'wrong'})
    assert status_code == 400
    assert validate(body_json, load_schema(WRONG_USERAGENT_SCHEMA)) is None


def test_student_without_authorization_header():
    status_code, body_json = get_response(
        'GET', URI, override_headers={'Authorization': None})
    assert status_code == 401
    assert validate(body_json, load_schema(WRONG_TOKEN_SCHEMA)) is None


def test_student_with_wrong_credentials():
    status_code, body_json = get_response(
        'GET', URI, send_wrong_credentials=True)
    assert status_code == 401
    assert validate(body_json, load_schema(WRONG_TOKEN_SCHEMA)) is None


def test_student_with_wrong_delete_method():
    status_code, body_json = get_response('DELETE', URI)
    assert status_code == 405
    assert validate(body_json, load_schema(DELETE_NOT_ALLOWED)) is None


def test_student_with_wrong_post_method():
    status_code, body_json = get_response('POST', URI)
    assert status_code == 405
    assert validate(body_json, load_schema(POST_NOT_ALLOWED)) is None


def test_student_with_wrong_put_method():
    status_code, body_json = get_response('PUT', URI)
    assert status_code == 405
    assert validate(body_json, load_schema(PUT_NOT_ALLOWED)) is None


def test_student_with_wrong_patch_method():
    status_code, body_json = get_response('PATCH', URI)
    assert status_code == 405
    assert validate(body_json, load_schema(PATCH_NOT_ALLOWED)) is None


def test_student_with_right_credentials():
    status_code, body_json = get_response('GET', URI)
    assert status_code == 200
    assert validate(body_json, load_schema(STUDENT_200_SCHEMA)) is None
